<?php

namespace App\Mailer;

use App\Entity\Absence;
use App\Entity\User;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class Mailer
{

    public function newAbsence(Absence $absence, array $recipients, $standin, MailerInterface $mailer)
    {
        foreach ($recipients as $recipient) {
            $user = $absence->getUser();
            $message = 'Hello ' . $recipient->getFirstName() . '. '
                . 'A new absence was added by ' . $user->getFirstName() . ' ' . $user->getLastName() . '.';

            if (!is_null($standin))
            {
                $message .= 'The designated stand-in is ' . $standin->getFirstName() . ' ' . $standin->getLastName() . '.';
            }

            if (!is_null($absence->getComment()))
            {
                $message .= $absence->getComment();
            }

            $email = (new EMail())
                ->from("informer@urlaubsplaner.de")
                ->to($recipient->getEmail())#
                ->subject("New Absence added by " . $user->getFirstName() . " " . $user->getLastName())
                ->text($message);

            $mailer->send($email);
        }
    }
}