<?php

namespace App\DataFixtures;

use App\Entity\Absence;
use App\Entity\Group;
use App\Entity\PublicHoliday;
use App\Entity\PublicHolidaySet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $holiday2022_12_24 = new PublicHoliday("2022-12-24", true);
        $manager->persist($holiday2022_12_24);
        $holiday2022_12_31 = new PublicHoliday("2022-12-31", true);
        $manager->persist($holiday2022_12_31);
        $holiday2022_01_01 = new PublicHoliday("2022-01-01", false);
        $manager->persist($holiday2022_01_01);
        $holiday2022_04_15 = new PublicHoliday("2022-04-15", false);
        $manager->persist($holiday2022_04_15);
        $holiday2022_04_18 = new PublicHoliday("2022-04-18", false);
        $manager->persist($holiday2022_04_18);
        $holiday2022_05_01 = new PublicHoliday("2022-05-01", false);
        $manager->persist($holiday2022_05_01);
        $holiday2022_05_26 = new PublicHoliday("2022-05-26", false);
        $manager->persist($holiday2022_05_26);
        $holiday2022_06_06 = new PublicHoliday("2022-06-06", false);
        $manager->persist($holiday2022_06_06);
        $holiday2022_06_16 = new PublicHoliday("2022-06-16", false);
        $manager->persist($holiday2022_06_16);
        $holiday2022_10_03 = new PublicHoliday("2022-10-03", false);
        $manager->persist($holiday2022_10_03);
        $holiday2022_11_01 = new PublicHoliday("2022-11-01", false);
        $manager->persist($holiday2022_11_01);
        $holiday2022_12_25 = new PublicHoliday("2022-12-25", false);
        $manager->persist($holiday2022_12_25);
        $holiday2022_12_26 = new PublicHoliday("2022-12-26", false);
        $manager->persist($holiday2022_12_26);

        $holidaySet = new PublicHolidaySet();
        $holidaySet->setName("NRW");
        $holidaySet->addDay($holiday2022_12_24);
        $holidaySet->addDay($holiday2022_12_31);
        $holidaySet->addDay($holiday2022_01_01);
        $holidaySet->addDay($holiday2022_04_15);
        $holidaySet->addDay($holiday2022_04_18);
        $holidaySet->addDay($holiday2022_05_01);
        $holidaySet->addDay($holiday2022_05_26);
        $holidaySet->addDay($holiday2022_06_06);
        $holidaySet->addDay($holiday2022_06_16);
        $holidaySet->addDay($holiday2022_10_03);
        $holidaySet->addDay($holiday2022_11_01);
        $holidaySet->addDay($holiday2022_12_25);
        $holidaySet->addDay($holiday2022_12_26);
        $manager->persist($holidaySet);

        $infraGroup = new Group();
        $infraGroup->setName("InfraDev");
        $manager->persist($infraGroup);

        $absence1 = new Absence();
        $absence1->setStart(\DateTime::createFromFormat("Y-d-m", "2022-02-10"));
        $absence1->setEnd(\DateTime::createFromFormat("Y-d-m", "2022-03-01"));
        $absence1->setType('U');
        $manager->persist($absence1);

        $absence2 = new Absence();
        $absence2->setStart(\DateTime::createFromFormat("Y-d-m", "2022-05-05"));
        $absence2->setEnd(\DateTime::createFromFormat("Y-d-m", "2022-05-25"));
        $absence2->setType('U');
        $manager->persist($absence2);


        $UserA = new User();
        $UserA->setUsername("peter");
        $UserA->setFirstName("Peter");
        $UserA->setLastName("Argh");
        $UserA->setEmail("Peter@mail.de");
        $UserA->setVacationDays(30);
        $UserA->setRoles(["ROLE_USER"]);
        $UserA->setPublicHolidaySet($holidaySet);
        $UserA->setUsergroup($infraGroup);
        $UserA->setPassword($this->passwordHasher->hashPassword($UserA,'abc'));
        $UserA->addAbsences($absence2);
        $manager->persist($UserA);

        $adminUser = new User();
        $adminUser->setUsername("admin");
        $adminUser->setFirstName("Armin");
        $adminUser->setLastName("Admin");
        $adminUser->setEmail("Armin@Admins.de");
        $adminUser->setVacationDays(30);
        $adminUser->setRoles(["ROLE_USER", "ROLE_ADMIN"]);
        $adminUser->setPublicHolidaySet($holidaySet);
        $adminUser->setUsergroup($infraGroup);
        $adminUser->setPassword($this->passwordHasher->hashPassword($adminUser,'admin'));
        $adminUser->addAbsences($absence1);
        $adminUser->addStandin($UserA);
        $manager->persist($adminUser);






        $manager->flush();
    }
}
