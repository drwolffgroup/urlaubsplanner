<?php

namespace App\Repository;

use App\Entity\PublicHolidaySet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PublicHolidaySet|null find($id, $lockMode = null, $lockVersion = null)
 * @method PublicHolidaySet|null findOneBy(array $criteria, array $orderBy = null)
 * @method PublicHolidaySet[]    findAll()
 * @method PublicHolidaySet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PublicHolidaySetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PublicHolidaySet::class);
    }

    // /**
    //  * @return PublicHolidaySet[] Returns an array of PublicHolidaySet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PublicHolidaySet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
