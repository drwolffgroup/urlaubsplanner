<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PublicHolidayRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;


#[ORM\Entity(repositoryClass: PublicHolidayRepository::class)]
#[ApiResource(
    collectionOperations: ["get"],
    itemOperations: ["get"]
)]
class PublicHoliday
{
    public function __construct(string $dateString, bool $halfDay)
    {
        echo($dateString);
        $this->setDate(DateTime::createFromFormat("Y-m-d", $dateString));
        $this->setHalfDay($halfDay);
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'date')]
    private $date;

    #[ORM\ManyToOne(targetEntity: PublicHolidaySet::class, inversedBy: 'days')]
    #[ORM\JoinColumn(nullable: false)]
    private $HolidaySet;

    #[ORM\Column(type: 'boolean')]
    #[Groups(["user:holidayset:read"])]
    private $halfDay;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    #[SerializedName("date")]
    #[Groups(["user:holidayset:read"])]
    public function getDateString(): string
    {
        return $this->date->format("Y-d-m");
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getHolidaySet(): ?PublicHolidaySet
    {
        return $this->HolidaySet;
    }

    public function setHolidaySet(?PublicHolidaySet $HolidaySet): self
    {
        $this->HolidaySet = $HolidaySet;

        return $this;
    }

    public function getHalfDay(): ?bool
    {
        return $this->halfDay;
    }

    public function setHalfDay(bool $halfDay): self
    {
        $this->halfDay = $halfDay;

        return $this;
    }
}
