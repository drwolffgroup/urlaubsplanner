<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PublicHolidaySetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[
    ORM\Entity(repositoryClass: PublicHolidaySetRepository::class)]
#[ApiResource(
    collectionOperations: [],
    itemOperations: ["get"],
    subresourceOperations: ['api_users_public_holiday_set_get_subresource' => [
        'method' => 'GET',
        'normalization_context' => [
            'groups' => ['user:holidayset:read'],
        ],
    ]
    ]
)]
class PublicHolidaySet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'HolidaySet', targetEntity: PublicHoliday::class)]
    private $days;

    //#[Groups(["user:read:as_subresource:holiday", "user:read"])]
    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    public function __construct()
    {
        $this->days = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|PublicHoliday[]
     */
    #[Groups(["user:holidayset:read"])]
    public function getDays(): Collection
    {
        return $this->days;
    }

    public function addDay(PublicHoliday $day): self
    {
        if (!$this->days->contains($day)) {
            $this->days[] = $day;
            $day->setHolidaySet($this);
        }

        return $this;
    }

    public function removeDay(PublicHoliday $day): self
    {
        if ($this->days->removeElement($day)) {
            // set the owning side to null (unless already changed)
            if ($day->getHolidaySet() === $this) {
                $day->setHolidaySet(null);
            }
        }

        return $this;
    }

    #[Groups(["user:holidayset:read"])]
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

}
