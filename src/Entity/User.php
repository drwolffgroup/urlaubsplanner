<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    collectionOperations: ["get"],
    itemOperations: ["get"],
    subresourceOperations: [
        'api_users_standins_get_subresource' => [
            'method' => 'GET',
            'normalization_context' => [
                'groups' => ['user:standin:read'],
            ],
        ]
    ],
    normalizationContext: ['groups' => ['user:read'], "enable_max_depth" => "true"],
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(["user:read"])]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups(["user:read", "user:standin:read"])]
    private $username;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Absence::class)]
    #[Groups(["user:read", "user:standin:read"])]
    private $absences;

    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'standins')]
    #[MaxDepth(1)]
    #[ApiSubresource(maxDepth: 1)]
    private $standins;

    #[ORM\ManyToOne(targetEntity: PublicHolidaySet::class)]
    #[MaxDepth(1)]
    #[ApiSubresource()]
    private $PublicHolidaySet;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["user:read", "user:standin:read"])]
    private $firstName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(["user:read", "user:standin:read"])]
    private $lastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(["user:read", "user:standin:read"])]
    private $email;

    #[ORM\Column(type: 'smallint')]
    #[Groups(["user:read"])]
    private $vacationDays;

    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'supervisors')]
    private $supervisedGroups;

    #[ORM\ManyToOne(targetEntity: group::class, inversedBy: 'members')]
    private $usergroup;

    public function __construct()
    {
        $this->absences = new ArrayCollection();
        $this->standins = new ArrayCollection();
        $this->supervisedGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string)$this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsences(Absence $absences): self
    {
        if (!$this->absences->contains($absences)) {
            $this->absences[] = $absences;
            $absences->setUser($this);
        }

        return $this;
    }

    public function removeAbsences(Absence $absences): self
    {
        if ($this->absences->removeElement($absences)) {
            // set the owning side to null (unless already changed)
            if ($absences->getUser() === $this) {
                $absences->setUser(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getUserIdentifier();
    }

    /**
     * @return Collection|self[]
     */
    public function getStandins(): Collection
    {
        return $this->standins;
    }

    public function addStandin(self $standin): self
    {
        if (!$this->standins->contains($standin)) {
            $this->standins[] = $standin;
        }

        return $this;
    }

    public function removeStandin(self $standin): self
    {
        $this->standins->removeElement($standin);

        return $this;
    }

    public function getPublicHolidaySet(): ?PublicHolidaySet
    {
        return $this->PublicHolidaySet;
    }

    public function setPublicHolidaySet(?PublicHolidaySet $PublicHolidaySet): self
    {
        $this->PublicHolidaySet = $PublicHolidaySet;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getVacationDays(): ?int
    {
        return $this->vacationDays;
    }

    public function setVacationDays(int $vacationDays): self
    {
        $this->vacationDays = $vacationDays;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getSupervisedGroups(): Collection
    {
        return $this->supervisedGroups;
    }

    public function addSupervisedGroup(Group $supervisedGroup): self
    {
        if (!$this->supervisedGroups->contains($supervisedGroup)) {
            $this->supervisedGroups[] = $supervisedGroup;
            $supervisedGroup->addSupervisor($this);
        }

        return $this;
    }

    public function removeSupervisedGroup(Group $supervisedGroup): self
    {
        if ($this->supervisedGroups->removeElement($supervisedGroup)) {
            $supervisedGroup->removeSupervisor($this);
        }

        return $this;
    }

    public function getUsergroup(): ?group
    {
        return $this->usergroup;
    }

    public function setUsergroup(?group $usergroup): self
    {
        $this->usergroup = $usergroup;

        return $this;
    }

}
