<?php

namespace App\DurationCalculator;

use DateInterval;
use DateTime;

class DurationCalculator
{
    public static function calculate(DateTime $start, DateTime $end, array $holidays, DateTime $cutOffStart = null, DateTime $cutOffEnd = null ) : int
    {
        if ($start > $end)
            return -1;

        $count = 0;
        $interval = DateInterval::createFromDateString('1 day');


        $date = clone $start;
        while($end >= $date)
        {
            if (!is_null($cutOffStart) and $date < $cutOffStart)
            {
                $date = date_add($date, $interval);
                continue;
            }


            if (!is_null($cutOffEnd) and $date > $cutOffEnd)
            {
                $date = date_add($date, $interval);
                continue;
            }


            $weekday = $date->format('w');

            if (!($weekday == 0) and !($weekday == 6))
            {
                $isHoliday = false;
                foreach($holidays as $holiday)
                {
                    if ($holiday->format("Y-m-d") == $date->format("Y-m-d"))
                    {
                        $isHoliday=true;
                        break;
                    }
                }

                if (!$isHoliday)
                    $count += 1;

            }
            $date = date_add($date, $interval);
        }
        return $count;
    }
}