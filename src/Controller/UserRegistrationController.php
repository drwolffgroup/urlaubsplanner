<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserRegistrationController extends AbstractController
{
    #[Route('/user/registration', name: 'user_registration')]
    public function index(ManagerRegistry $doctrine, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $registrationForm = $this->createFormBuilder()
            ->add('username', TextType::class,[
                'label' => 'Username'
            ])
            ->add('password', RepeatedType::class,[
                'type' => PasswordType::class,
                'required' => true,
                'first_options' => ['label' => 'Enter password.'],
                'second_options' => ['label' => 'Repeat password.']
            ])
            ->add('Register', SubmitType::class)
            ->getForm();

        $registrationForm->handleRequest($request);

        if ($registrationForm->isSubmitted())
        {
            $input = $registrationForm->getData();

            $user = new User();
            $user->setUsername($input['username']);
            $user->setPassword($passwordHasher->hashPassword($user, $input['password']));
            $em = $doctrine->getManager();
            $em->persist($user);
            $em->flush();

            $this->redirect($this->generateUrl('app_login'));
        }

        return $this->render('user_registration/index.html.twig', [
            'registrationForm' => $registrationForm->createView()
        ]);
    }
}
