<?php

namespace App\Controller;

use App\DurationCalculator\DurationCalculator;
use App\Entity\Absence;
use App\Entity\User;
use App\Form\AbsenceType;
use App\Mailer\Mailer;
use DateTime;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(Request $request, ManagerRegistry $doctrine, UserInterface $loggedInUser = null, MailerInterface $mailer): Response
    {
        if (!isset($loggedInUser)) {
            return $this->redirect($this->generateUrl('app_login'));
        }

        $year = $request->query->get('year');

        if (!isset($year))
            $year = 2022;

        $minDate = DateTime::createFromFormat('Y-m-d', $year . "-01-01");
        $maxDate = DateTime::createFromFormat('Y-m-d', $year . "-12-31");


        $userId = $loggedInUser->getId();
        $user = $doctrine->getManager()->getRepository(User::class)->find($userId);

        $userinfo = [
            'lastname' => $user->getFirstName(),
            'username' => $user->getUserIdentifier(),
            'groupname' => $user->getUsergroup()->getName()
        ];

        $supervisors = [];
        $supervisorsUserArray = [];
        $group = $user->getUsergroup();
        if(!is_null($group)){
            foreach ($group->getSupervisors() as $supervisor)
            {
                $supervisorsUserArray[] = $supervisor;
                $supervisors[] = [
                    "first_name" => $supervisor->getFirstName(),
                    "last_name" => $supervisor->getLastName(),
                    "email" => $supervisor->getEmail()
                ];
            }
        }


        $newAbsence = new Absence();
        $absenceForm = $this->createForm(AbsenceType::class, $newAbsence);
        $absenceForm->handleRequest($request);
        if ($absenceForm->isSubmitted())
        {
            $newAbsence->setUser($user);
            if (is_null($newAbsence->getEnd()))
            {
                $newAbsence->setEnd(clone $newAbsence->getStart());
            }

            $em = $doctrine->getManager();
            $em->persist($newAbsence);
            $em->flush();

            $calendarMailer = new Mailer();
            $calendarMailer->newAbsence($newAbsence, $supervisorsUserArray, null, $mailer);
        }

        $criteriaEnd = Criteria::create()->where(Criteria::expr()->gte("end", $minDate));
        $criteriaStart = Criteria::create()->where(Criteria::expr()->lte("start", $maxDate));
        $absences = $user->getAbsences()->matching($criteriaEnd)->matching($criteriaStart);

        $publicHolidaySet = $user->getPublicHolidaySet();
        $holidays = $publicHolidaySet->getDays()->getValues();
        $holidaysArray = [];
        $holidaysStringArray = [];
        foreach ($holidays as $item) {
            if ($item->getDate() >= $minDate and $item->getDate() <= $maxDate) {
                $holidaysArray[] = $item->getDate();
                $holidaysStringArray[] = $item->getDate()->format('Y-m-d');
            }
        }



        $durationThisYear = 0;

        $tableData = [];
        foreach ($absences as $absence) {
            $tableRow = [
                'start' => $absence->getStart()->format('Y-m-d'),
                'type' => $absence->getType(),
                'end' => null,
                'duration' => '1',
                'durationThisYear' => '1',
                'comment' => $absence->getComment(),
                'id' => $absence->getId()
            ];
            if (!is_null($absence->getEnd())) {
                $tableRow['end'] = $absence->getEnd()->format('Y-m-d');

                $duration = DurationCalculator::calculate($absence->getStart(), $absence->getEnd(), $holidaysArray);
                $absenceDurationThisYear = DurationCalculator::calculate($absence->getStart(), $absence->getEnd(), $holidaysArray, $minDate, $maxDate);
                $tableRow['duration'] = $duration;
                $tableRow['durationThisYear'] = $absenceDurationThisYear;

                $durationThisYear += $absenceDurationThisYear;
            } else {
                $durationThisYear += 1;
            }
            $tableData[] = $tableRow;
        }

        $standins = array();
        foreach ($user->getStandins() as $standin) {
            $standinAbsences = $standin->getAbsences()->matching($criteriaEnd)->matching($criteriaStart);;
            $absenceList = [];

            foreach ($standinAbsences as $absence) {
                $tableRow = [
                    'start' => $absence->getStart()->format('Y-m-d'),
                    'type' => $absence->getType(),
                    'end' => null
                ];
                if (!is_null($absence->getEnd())) {
                    $tableRow['end'] = $absence->getEnd()->format('Y-m-d');

                }
                $absenceList[] = $tableRow;
            }
            $standins[$standin->getUserIdentifier()] = [
                'absences' => $absenceList,
                'first_name' => $standin->getFirstName(),
                'last_name' => $standin->getLastName(),
            ];
        }

        $yearOverview = [
            'vacationDays' => $user->getVacationDays(),
            'plannedVacation' => $durationThisYear
        ];





        return $this->render('home/index.html.twig', [
            'controller_name' => 'MainController',
            'absences' => $tableData,
            'year' => $year,
            'standins' => $standins,
            'holidays' => $holidaysStringArray,
            'yearOverview' => $yearOverview,
            'userinfo' => $userinfo,
            'absenceForm' => $absenceForm->createView(),
            'supervisors' => $supervisors,
            'user_id' => $user->getId()
        ]);
    }

    #[Route('/remove/{id}', name: 'remove')]
    public function removeAbsence($id, ManagerRegistry $doctrine, UserInterface $loggedInUser = null): Response
    {
        var_dump($id);
        $em = $doctrine->getManager();
        $absence = $em->find(Absence::class, $id);
        $em->remove($absence);
        $em->flush();

        return $this->redirect($this->generateUrl('home'));
    }
}
