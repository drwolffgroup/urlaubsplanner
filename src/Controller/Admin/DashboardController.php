<?php

namespace App\Controller\Admin;

use App\Entity\Absence;
use App\Entity\Group;
use App\Entity\PublicHoliday;
use App\Entity\PublicHolidaySet;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // redirect to some CRUD controller
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(UserCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Urlaubsplaner Admin');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Users', 'fas fa-list', User::class);
        yield MenuItem::linkToCrud('Absences', 'fas fa-list', Absence::class);
        yield MenuItem::linkToCrud('Groups', 'fas fa-list', Group::class);
        yield MenuItem::section('Public Holiday Config');
        yield MenuItem::linkToCrud('PublicHolidaySet', 'fas fa-list', PublicHolidaySet::class);
        yield MenuItem::linkToCrud('PublicHoliday', 'fas fa-list', PublicHoliday::class);
    }
}
