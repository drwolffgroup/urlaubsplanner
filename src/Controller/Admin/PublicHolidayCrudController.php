<?php

namespace App\Controller\Admin;

use App\Entity\PublicHoliday;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;

class PublicHolidayCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PublicHoliday::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateField::new('date'),
            BooleanField::new('halfDay'),
            AssociationField::new('HolidaySet')
        ];
    }

}
