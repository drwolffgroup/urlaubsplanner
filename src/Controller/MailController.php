<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailController extends AbstractController
{
    #[Route('/mail', name: 'mail')]
    public function sendMail(MailerInterface $mailer): Response
    {
        $email = (new EMail())
            ->from("informer@urlaubsplaner.de")
            ->to("christian.hoelscher@drwolffgroup.com")#
            ->subject("Calendar Info")
            ->text("test");

        $mailer->send($email);

        return new Response("Sent mail.");
    }
}
