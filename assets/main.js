import Vue from 'vue'
import './plugins/axios'
import vuetify from './plugins/vuetify'
import './plugins/veevalidate'
import i18n from './plugins/i18n'
import App from './App.vue'
import router from './router'
import { store } from './store'
import VuetifyConfirm from 'vuetify-confirm'
import VCalendar from 'v-calendar'

Vue.config.productionTip = false
Vue.use(VuetifyConfirm, { vuetify })
Vue.use(VCalendar, {
  componentPrefix: 'vc'
})

const app = new Vue({
  vuetify,
  router,
  store,
  i18n,
  data() {
    return {
      userid: '',
    };
  },
  beforeMount: function () {
    this.userid = this.$el.attributes['user_id'].value;
  },
  render: (h) => h(App)
}).$mount('#app')

if (window.Cypress) {
  // Only available during E2E tests
  window.app = app
}
