import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/src/styles/main.sass'
// import colors from 'vuetify/es5/util/colors'
// import VuetifyConfirm from 'vuetify-confirm'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify)

const opts = {
  theme: {
    themes: {
      light: {
        primary: '#00305d',
        accent: '#a81e1e',
        secondary: '#26ff8c',
        success: '#a5d64c',
        info: '#ff53d0',
        warning: '#ff8e00',
        error: '#ff5252',
        white: '#ffffff',
        neutral: '#6f6f6f'
      }
    }
  },
  iconfont: 'mdi'
}

export default new Vuetify(opts)
