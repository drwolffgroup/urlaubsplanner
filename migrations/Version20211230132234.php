<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211230132234 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD public_holiday_set_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64975294D04 FOREIGN KEY (public_holiday_set_id) REFERENCES public_holiday_set (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64975294D04 ON user (public_holiday_set_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64975294D04');
        $this->addSql('DROP INDEX IDX_8D93D64975294D04 ON user');
        $this->addSql('ALTER TABLE user DROP public_holiday_set_id');
    }
}
