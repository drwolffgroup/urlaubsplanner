<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211230131424 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE public_holiday (id INT AUTO_INCREMENT NOT NULL, holiday_set_id INT NOT NULL, date DATE NOT NULL, INDEX IDX_8744ED8615D2E034 (holiday_set_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE public_holiday_set (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE public_holiday ADD CONSTRAINT FK_8744ED8615D2E034 FOREIGN KEY (holiday_set_id) REFERENCES public_holiday_set (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE public_holiday DROP FOREIGN KEY FK_8744ED8615D2E034');
        $this->addSql('DROP TABLE public_holiday');
        $this->addSql('DROP TABLE public_holiday_set');
    }
}
